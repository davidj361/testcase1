#ifndef GUARD_MOVIENODE_H
#define GUARD_MOVIENODE_H

#include "Movie.h"

template <typename T>
class Node
{
        // Friendships
        template <class A>
        friend class List;

        public:
                // constructor
                Node(T t);

                // getters
                T data;
};

#endif
